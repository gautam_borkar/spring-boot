package com.psl.course;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {
	
	@Autowired
	private CourseRepository courseRepository;
	
	public List<Course> getAllCourse(int topicId) {
		List<Course> Courses = new ArrayList<Course>();
		courseRepository.findByTopicId(topicId).forEach(Courses::add);
		return Courses;
	}
	
	public Course getCourse(int id) {
		return courseRepository.findById(id).orElse(null);
	}

	public void addCourse(Course course) {
		courseRepository.save(course);
	}

	public void updateCourse(int id, Course course) {
		courseRepository.save(course);
	}

	public void deleteCourse(int id) {
		courseRepository.deleteById(id);
	}
}
